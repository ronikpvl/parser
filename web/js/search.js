var Search = function(options){
    this.initControls();
}

Search.prototype.initControls = function(options){
    var obj = this;
    obj.control = {}
    obj.helper  = {}

    obj.control.loader       = $('.button-wrapper').find('img');
    obj.control.priceFrom    = $("#parser_search_priceFrom");
    obj.control.priceTo      = $("#parser_search_priceTo");
    obj.control.rooms        = $("#parser_search_rooms");
    obj.control.stations     = $("#parser_search_stations");
    obj.control.tableWrapper = $(".search-form-result");

    obj.helper.notFoundTemplate = function(options){
        return '<div>Ничего не найдено</div>';
    }

    obj.helper.requestError = function(options){
        alert('Произошла ошибка');
    }

    obj.helper.tableWrapperClean = function(options){
        obj.control.tableWrapper.html('');
    }

    obj.helper.loader = function(status){
        if (status){
            obj.control.loader.show();
        }
        else{
            obj.control.loader.hide();
        }
    }
}

Search.prototype.searchRequest = function(options){
    var obj = this;

    obj.helper.loader(true);

    $.ajax({
        url   : '/search/json',
        type  : 'POST',
        async : true,
        dataType : 'html',
        data : {
            priceFrom : obj.control.priceFrom.val(),
            priceTo   : obj.control.priceTo.val(),
            rooms     : obj.control.rooms.val(),
            stations  : obj.control.stations.val()
        },
        success : function(response){
            obj.helper.tableWrapperClean();
            obj.control.tableWrapper.append(response ? response : obj.helper.notFoundTemplate());
            obj.helper.loader(false);
        },
        error : function(){
            obj.helper.requestError();
            obj.helper.loader(false);
        }
    });
}