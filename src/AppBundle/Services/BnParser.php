<?php

/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 06.11.2016
 * Time: 13:52
 */

namespace AppBundle\Services;

use AppBundle\Helpers\FileManager;
use Curl\Curl;
use AppBundle\Helpers\Parser;
use Symfony\Component\HttpKernel\Kernel;

class BnParser
{
    const HOST = 'http://www.bn.ru';
    const USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1';

    private $kernel;

    function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Return cached stations
     *
     * @return array|bool
     */
    public function getStations()
    {
        $fileManager = new FileManager();
        $fileManager->setDir($this->kernel->getRootDir() . '/../web/cache/');
        $fileManager->setName('stations.txt');

        if ($fileManager->fileExist()){
            return (array) json_decode($fileManager->getFileContent());
        }

        $curl = new Curl();
        $curl->setUserAgent(self::USER_AGENT);
        $curl->get(self::HOST . '/zap_fl_w2.phtml');
        $curl->close();

        if ($curl->error){
            return false;
        }

        $parseResponse = Parser::parseStations($curl->response);

        if (!$parseResponse){
            return false;
        }

        $fileManager->createFile(json_encode($parseResponse));

        return (array) $parseResponse;
    }

    /**
     * Return parsed items
     *
     * @param $options
     * @return bool
     */
    public function getFlats($options)
    {
        $curl = new Curl();
        $curl->setUserAgent(self::USER_AGENT);
        $curl->get(self::HOST . '/zap_fl.phtml', self::convertRequestOptions($options));
        $curl->close();

        if ($curl->error){
            return false;
        }

        return Parser::parseItems($curl->response);
    }

    /**
     * Set valid options for request
     *
     * @param $options
     * @return array
     */
    private static function convertRequestOptions($options)
    {
        $options = array_filter($options);

        $optionsDefault = [
            'priceFrom' => null,
            'priceTo'   => null,
            'stations'  => [],
            'rooms'     => [1 => 0, 5 => 0]
        ];

        $options = $options + $optionsDefault;

        $optionsReady = [];

        $optionsReady['price1'] = $options['priceFrom'];
        $optionsReady['price2'] = $options['priceTo'];
        $optionsReady['kkv2']   = max($options['rooms']);
        $optionsReady['kkv1']   = min($options['rooms']);
        $optionsReady['metro']  = $options['stations'];

        return $optionsReady;
    }
}
