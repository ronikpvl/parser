<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 07.11.2016
 * Time: 1:45
 */

namespace AppBundle\DataObjects;

use AppBundle\Helpers\PropertyEncoder;


class Flat extends PropertyEncoder
{
    protected $rooms;
    protected $address;
    protected $floor;
    protected $buildType;
    protected $phone;
    protected $bathroom;
    protected $price;
    protected $subject;
    protected $info;
    protected $area;

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $rooms) : $rooms;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $address) : $address;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param mixed $floor
     */
    public function setFloor($floor)
    {
        $this->floor = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $floor) : $floor;
    }

    /**
     * @return mixed
     */
    public function getBuildType()
    {
        return $this->buildType;
    }

    /**
     * @param mixed $buildType
     */
    public function setBuildType($buildType)
    {
        $this->buildType = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $buildType) : $buildType;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $phone) : $phone;
    }

    /**
     * @return mixed
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * @param mixed $bathroom
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $bathroom) : $bathroom;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $price) : $price;
        $this->price = preg_replace("/[^0-9]/", "", $this->price);
        $this->price = $this->price * 1000;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $subject) : $subject;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $this->needConvert ? iconv($this->fromEncoding, $this->toEncoding, $info) : $info;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea(FlatArea $area)
    {
        $this->area = $area;
    }
}