<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 07.11.2016
 * Time: 1:49
 */

namespace AppBundle\DataObjects;


class FlatArea
{
    private $all;
    private $live;
    private $kitchen;

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->all;
    }

    /**
     * @param mixed $all
     */
    public function setAll($all)
    {
        $this->all = round($all);
    }

    /**
     * @return mixed
     */
    public function getLive()
    {
        return $this->live;
    }

    /**
     * @param mixed $live
     */
    public function setLive($live)
    {
        $this->live = round($live);
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @param mixed $kitchen
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = round($kitchen);
    }
}