<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $parser   = $this->get('app_bn_parser');
        $stations = $parser->getStations();

        $form = $this->createForm('AppBundle\Form\ParserSearchType', null, ['stations' => $stations]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $items = $parser->getFlats([
                'priceFrom' => $form->get('priceFrom')->getData(),
                'priceTo'   => $form->get('priceTo')->getData(),
                'rooms'     => $form->get('rooms')->getData(),
                'stations'  => $form->get('stations')->getData()
            ]);
        }

        return $this->render('AppBundle:Search:index.html.twig', array(
            'stations' => $stations,
            'form' => $form->createView(),
            'items' => !empty($items) ? $items : []
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function jsonSearchAction(Request $request)
    {
        $parser = $this->get('app_bn_parser');
        $items  = $parser->getFlats([
            'priceFrom' => $request->request->get('priceFrom'),
            'priceTo'   => $request->request->get('priceTo'),
            'rooms'     => $request->request->get('rooms'),
            'stations'  => $request->request->get('stations')
        ]);

        return $this->render('AppBundle:Search:form.result.table.html.twig', ['items' => $items]);
    }
}
