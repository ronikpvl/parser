<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 07.11.2016
 * Time: 1:57
 */

namespace AppBundle\Helpers;


class PropertyEncoder
{
    protected $fromEncoding;
    protected $toEncoding;
    protected $needConvert = false;

    /**
     * @return mixed
     */
    public function getFromEncoding()
    {
        return $this->fromEncoding;
    }

    /**
     * @param mixed $fromEncoding
     */
    public function setFromEncoding($fromEncoding)
    {
        $this->fromEncoding = $fromEncoding;
    }

    /**
     * @return mixed
     */
    public function getToEncoding()
    {
        return $this->toEncoding;
    }

    /**
     * @param mixed $toEncoding
     */
    public function setToEncoding($toEncoding)
    {
        $this->toEncoding = $toEncoding;
    }

    /**
     * @return boolean
     */
    public function isNeedConvert()
    {
        return $this->needConvert;
    }

    /**
     * @param boolean $needConvert
     */
    public function setNeedConvert($needConvert)
    {
        $this->needConvert = $needConvert;
    }
}