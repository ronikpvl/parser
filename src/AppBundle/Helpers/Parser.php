<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 05.11.2016
 * Time: 22:28
 */

namespace AppBundle\Helpers;

use phpQuery;
use AppBundle\DataObjects\Flat;
use AppBundle\DataObjects\FlatArea;
use AppBundle\DataObjects\Station;

class Parser
{

    /**
     * Parse metro stations and return array with key => value
     *
     * @param $data
     * @return array|bool
     */
    public static function parseStations($data)
    {
        if (!$data){
            return false;
        }

        $result = [];

        $document = phpQuery::newDocument($data);
        $options  = $document->find('#metro > option');

        if (!$options){
            return false;
        }

        foreach($options as $val){
            $pq = pq($val);

            $id   = $pq->attr('value');
            $name = $pq->text();

            if ($id < 1 || !$name){
                continue;
            }

            $result[$id] = $name;
        }

        return $result;
    }


    /**
     * Parse items
     *
     * @return bool
     */
    public static function parseItems($data)
    {
        if (!$data){
            return false;
        }

        $result = [];

        $document = phpQuery::newDocument($data);
        $items  = $document->find('.results > tr');

        if (!$items){
            return false;
        }

        foreach($items as $val){
            $pq = pq($val);
            $td = $pq->find('td');

            if (!in_array($td->size(), [10,14])){
                continue;
            }

            $flat     = new Flat();
            $flatArea = new FlatArea();

            $flat->setNeedConvert(true);
            $flat->setFromEncoding('CP1251');
            $flat->setToEncoding('UTF-8');

            if ($td->size() == 14){
                $flat->setRooms($td->eq(1)->html());
                $flat->setAddress($td->eq(2)->find('a')->html());
                $flat->setFloor($td->eq(3)->html());
                $flat->setBuildType($td->eq(4)->find('a')->html());
                $flat->setPhone($td->eq(12)->html());
                $flat->setBathroom($td->eq(8)->html());
                $flat->setPrice($td->eq(9)->find('div')->find('b')->html());
                $flat->setSubject($td->eq(11)->html());
                $flat->setInfo($td->eq(13)->html());

                $flatArea->setAll($td->eq(5)->html());
                $flatArea->setKitchen($td->eq(7)->html());
                $flatArea->setLive($td->eq(6)->html());

                $flat->setArea($flatArea);
            }
            elseif ($td->size() == 10){
                $flat->setRooms('--');
                $flat->setAddress($td->eq(1)->find('a')->html());
                $flat->setFloor($td->eq(2)->html());
                $flat->setBuildType($td->eq(3)->find('a')->html());
                $flat->setPhone($td->eq(8)->html());
                $flat->setBathroom('--');
                $flat->setPrice($td->eq(6)->find('strong')->html());
                $flat->setSubject($td->eq(7)->find('a')->html());
                $flat->setInfo($td->eq(9)->html());

                $flatArea->setAll($td->eq(4)->html());
                $flatArea->setLive($td->eq(5)->find('span')->html());
                $flatArea->setKitchen(0);

                $flat->setArea($flatArea);
            }

            $result[] = $flat;
        }

        return $result;
    }
}