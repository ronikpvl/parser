<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 07.11.2016
 * Time: 15:22
 */

namespace AppBundle\Helpers;


class FileManager
{
    private $dir;
    private $name;

    /**
     * @return mixed
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param mixed $dir
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Return file content
     *
     * @param array $options
     * @return bool|string
     */
    public function getFileContent($options = [])
    {
        if (!$this->dir || !$this->name){
            return false;
        }

        return file_get_contents($this->dir . $this->name);
    }

    /**
     * Create empty file or get content from $content
     *
     * @param null $content
     * @param array $options
     * @return bool
     */
    public function createFile($content = null, $options = [])
    {
        if (!$this->dir || !$this->name){
            return false;
        }

        $options_default = [
            'rules' => 0700
        ];

        $options = $options + $options_default;

        mkdir($this->dir, $options['rules']);

        return file_put_contents($this->dir . $this->name, $content) === 0 ? true : false;
    }

    /**
     *  Clear object
     */
    public function clear()
    {
        $this->dir  = null;
        $this->name = null;
    }

    /**
     * Checking file exist
     *
     * @return bool
     */
    public function fileExist()
    {
        if (!$this->dir || !$this->name){
            return false;
        }

        return is_readable($this->dir . $this->name);
    }
}