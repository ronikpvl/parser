<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ParserSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $stations = $options['stations'] ? $options['stations'] : [];

        $builder
            ->add('priceFrom', TextType::class, [
                'label'    => 'Цена (тыс руб)',
                'mapped'   => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 1000
                ]
            ])
            ->add('priceTo', TextType::class, [
                'label'    => 'Цена (тыс руб)',
                'mapped'   => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 4500
                ]
            ])
            ->add('rooms', ChoiceType::class, [
                'label'    => 'Комнаты',
                'mapped'   => false,
                'multiple' => true,
                'required' => false,
                'attr'     => ['size' => 7],
                'choices'  => $options['rooms']
            ])
            ->add('stations', ChoiceType::class, [
                'label'    => 'Станция метро',
                'multiple' => true,
                'mapped'   => false,
                'required' => false,
                'choices'  => $stations,
                'attr'     => ['size' => 7]
            ]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'stations' => [],
            'rooms' => [
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => '5+',
            ]
        ));
    }
}
